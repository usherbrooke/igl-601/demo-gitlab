import Vue from 'vue';
import Hello from '../../src/components/Hello.vue';

describe('Hello World', () => {
  it('Correctly sets the Hello World msg', () => {
    const vm = new Vue(Hello).$mount();
    expect(vm.msg).to.equal('Hello world!');
  });
});
