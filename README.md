# Démonstration de GitLab - UDS IGL601

## À propos

### Description

L'objectif de ce dépôt de code est de présenter GitLab et de survoler les fonctionnalités suivantes:

*  Issues
*  Milestones
*  CI / CD

Ce projet à vu le jour dans le cadre du cours IGL601 enseigné par Alex Boulanger lors de la session d'automme 2017 à l'Université de Sherbrooke.

### Licence

Ce projet est disponible et distribué sous la licence GPL: https://www.gnu.org/licenses/gpl.html

Il est donc Open Source, mais le crédit doit être donné aux auteurs de ce dépôt.

### Auteurs

Le code a été produit par Mathieu Le Reste dans le cadre de son travail chez Propulso: https://propulso.io.

Le projet a été revu par Alex Boulanger.



## Environnement de développement

### Installation des dépendances

Pour ce projet, la gestion des dépendances se fait avec Yarn. Site web officiel du projet: https://yarnpkg.com.

Voici la commande pour installer localement les dépendances.

```
yarn
```

### Tests unitaires

```
yarn test
```

### Analyseur statique

Deux linters sont disponibles pour vérifier le JS et le SCSS. Pour les exécuter, utiliser la commande suivante:

```
yarn lint 
```

### Compilation, minification et transpilage

Webpack et Babel sont utilisés. Le dossier contenant le code compilé est `/dist`. La commande est la suivante:

```
yarn build
```

### Serveur local de développement

Un serveur local de développement avec des hot reload est disponible. Pour le lancer, il suffit d'utiliser la commande suivante:

```
yarn start
```